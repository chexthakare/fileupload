<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<div>
		<?php echo @$error;?>
		<!-- Error Message will show up here -->
		<?php echo form_open_multipart( 'Welcome/do_upload');?>
		<?php echo "<input type='file' name='userfile' size='20' />"; ?>
		<?php echo "<input type='submit' name='submit' value='upload' /> ";?>
		<?php echo "</form>"?>
		</div>

		<div>
			<ul>
			<?php foreach ($upload_data as $item => $value){ ?>

			<li><?php echo $item;?>: <?php echo $value;?></li>
			<?php $imgName = $upload_data['file_name'];?>
			<?php } ?>

			<li><?php echo "<h3>Uploaded file:</h3>"; ?></li>
			<li>
			  <img style="height:100px; width:100px;" src="<?php echo base_url().'/uploads/'.$imgName; ?>" /></li>
			</ul>
		</div>

	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	
</div>



</body>
</html>